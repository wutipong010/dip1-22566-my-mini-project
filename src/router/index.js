import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../views/HomeView.vue"),
    },
    {
      path: "/products",
      name: "products",
      component: () => import("../views/Products.vue"),
    },
    {
      path: "/products/Acer-Nitro-V-15-ANV15-51-578S",
      name: "Acer-Nitro-V-15-ANV15-51-578S",
      component: () => import("../components/Acer-Nitro-V-15-ANV15-51-578S.vue"),
    },
    {
      path: "/products/Acer-Nitro-v-15-ANV15-51-574G",
      name: "Acer-Nitro-v-15-ANV15-51-574G",
      component: () => import("../components/Acer-Nitro-v-15-ANV15-51-574G.vue"),
    },
    {
      path: "/products/Acer-Nitro-16-AN16-41-R6VJ",
      name: "Acer-Nitro-16-AN16-41-R6VJ",
      component: () => import("../components/Acer-Nitro-16-AN16-41-R6VJ.vue"),
    },
    {
      path: "/products/Acer-Nitro-16-AN16-41-R846",
      name: "Acer-Nitro-16-AN16-41-R846",
      component: () => import("../components/Acer-Nitro-16-AN16-41-R846.vue"),
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/LoginView.vue"),
    },
    {
      path: "/signup",
      name: "signup",
      component: () => import("../views/SignupView.vue"),
    },
    {
      path: "/:pathMatch(.*)",
      name: "NotFound",
      component: () => import("../views/NotFound.vue"),
    },
  ],
});

export default router;
