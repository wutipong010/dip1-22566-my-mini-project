import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { createHead } from '@unhead/vue'

const app = createApp(App)
const head = createHead()

app.use(createPinia())
app.use(head)
app.use(router)

app.mount('#app')
